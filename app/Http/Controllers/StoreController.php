<?php

namespace App\Http\Controllers;

use App\Models\Reception;
use App\Models\Refrigerator;
use App\Models\Store;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class StoreController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $recept = Reception::where('id', $id)->first();
        $recept_product = json_decode($recept->products, true);

        dump($recept_product);

        $true = [];
        $false = [];
        for($i=0;$i<count($recept_product);$i++){
            $product = DB::table('refrigerators')->where('name',$recept_product[$i]['name'])->first();

            if($product){
                $true[$i]['true'] = $recept_product[$i]['name'];
                if($recept_product[$i]['weight'] == 'г'){
                    $recept_product[$i]['all'] = (int)$recept_product[$i]['all'] / 1000;
                }
                if($recept_product[$i]['weight'] == 'л'){
                    $recept_product[$i]['all'] = (int)$recept_product[$i]['all'] / 1000;
                }
                $true[$i]['true_all'] = $recept_product[$i]['all'];
            }else{
                $false[] = $recept_product[$i]['name'].' '.$recept_product[$i]['all'].'-'.$recept_product[$i]['weight'];
                //$false[$i]['false_all'] = ;
            }
        }

        dump($true);
        dump($false);

        for($i=0;$i<count($true);$i++) {
            $prod = DB::table('refrigerators')->where('name',$true[$i]['true'])->first();
            dump($prod);
            if ($prod->weight == 'г'){
                $prod->weight = 'кг';
                $prod->all = (int)$prod->all / 1000;
            }
            if ($prod->weight == 'мл'){
                $prod->weight = 'л';
                $prod->all = (int)$prod->all / 1000;
            }
            //if ($prod->weight == 'ш') $prod->weight='ш';
            dump($prod->weight);
            $false[] = $prod->name.' '.($true[$i]['true_all']-$prod->all).'-'.$recept_product[$i]['weight'] ;
        }
        dump($false);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }
}
